﻿using System;
using System.Collections.Generic;

namespace TransportProj
{
    // Specifies the type of car we are driving
    public enum CarType { Sedan, RaceCar}

    public class Point {
        public int x;
        public int y;

        public Point() { }
        public Point(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }
        public int Speed { get; protected set; }
        private List<Point> waypoints = new List<Point>();
                 
        public Car(int xPos, int yPos, City city, Passenger passenger, int speed = 1)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
            Speed = speed;
        }

        public virtual void SetSpeed(int speed)
        {
            Speed = speed;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public static Car CarFactory(int xPos, int yPos, City city, Passenger passenger, CarType type)
        {
            if (type == CarType.RaceCar)
                return new RaceCar(xPos, yPos, city, passenger);
            else
                return new Sedan(xPos, yPos, city, passenger);
        }

        public abstract void MoveUp();

        public abstract void MoveDown();

        public abstract void MoveRight();

        public abstract void MoveLeft();

        public void LogPosition()
        {
            waypoints.Add(new Point(XPos, YPos));
        }

        public Point [] GetWaypoints()
        {
            return waypoints.ToArray();
        }
    }
}
