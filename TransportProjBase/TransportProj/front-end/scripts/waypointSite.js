
// Loads the trips via mongo REST API call ordered newest to oldest and sets the newest trip
function loadTrips(){
    
    $.get('https://api.mongolab.com/api/1/databases/2pointb-gauntlet/collections/trips?q={}&s={"_id": -1}&apiKey=dEspPSsJIqlgGtEo-ATBidGMkM3XRu-B',
       function (response) {
            tripArray = response;
            setTrip(tripArray[0]);
        })
        .fail(function () {
         alert("Server error");
       })
}


// Initializes the paper elements for trip
var tripSet = false;
var currentTrip;
function setTrip(trip, callback){
    
    // Clean up old elements
    if (typeof passenger !== 'undefined')
        passenger.remove();
    if (typeof destination !== 'undefined')
        destination.remove(); 
    if (typeof car !== 'undefined')
        car.remove();
    
    // Show passenger
    passenger = paper.image("images/passenger.png", 25 + trip.passengerStart.x * 50, 10 + trip.passengerStart.y * 50, 50, 50);
    
    // Wait and show destination
    setTimeout(function(){
      destination = paper.image("images/dest.png", 25 + trip.passengerDestination.x * 50, 10 + trip.passengerDestination.y * 50, 50, 50);
      
      // Wait and show car
      setTimeout(function(){
          
          // Create large circle
          car = paper.circle(50 + trip.carStart.x * 50, 50 + trip.carStart.y*50, 100);
          
          // Shrink circle and show 'run' button on complete
          car.attr({stroke: "#fff", "stroke-width": 4}).animate({r:10, callback: ()=>{$("#run").show(); if(callback) callback();}}, 400);
        }, 500);
    }, 500);  
    
    currentTrip = trip;
    tripSet = true;
}

// Called when the trip run is completed
function onTripComplete(){
    destination.attr({src: "images/dest_reached.png"});
    $("#run").html("Run Again");
    $("#run").show();
}

// Run through a trip
function doTrip(){
    
    // Trip is no longer setup
    $("#run").hide();
    tripSet = false;
    
    // Handle case when passenger starts on car
    if(currentTrip.passengerStart.x == currentTrip.carStart.x &&
       currentTrip.passengerStart.y == currentTrip.carStart.y   )
        passenger.remove();   
    
    // Create wrapped method for each waypoint
    onWaypoint = function (x, y, pickup, reachedDest) {
        return function () {
            
            // Highlight this waypoint
            grid[x][y].circle.attr({fill: "#fff", r: 12}).animate({fill: "#666", r: 8}, 500);
            
            // Handle special waypoints
            if((pickup) && (typeof passenger !== 'undefined'))
                passenger.remove();   
            if(reachedDest)
                onTripComplete();
        };
    };

    // Calculate time for total travel and percentage each waypoint takes in animation
    var totalTime = tripArray[0].waypoints.length * 500;
    var percentageLength = 100.0 / tripArray[0].waypoints.length; 

    // Construct animation array for each axis
    var yAnimation = {};
    var xAnimation = {};
    for(var i=0; i < currentTrip.waypoints.length; i++){
        var waypoint = currentTrip.waypoints[i];
        
        // Check if this waypoint is special
        var passengerPickup = false;
        var reachedDest = false;
        if((waypoint.x == currentTrip.passengerStart.x) &&
           (waypoint.y == currentTrip.passengerStart.y)   )
            passengerPickup = true;
        if(i == currentTrip.waypoints.length - 1)
            reachedDest = true;
        
        // Create animation to this waypoint on each axis
        yAnimation[(percentageLength * (i + 1)).toString() + "%"] = 
            {cy: grid[waypoint.x][waypoint.y].pos.y, easing: "linear", 
             callback: onWaypoint(waypoint.x, waypoint.y, passengerPickup, reachedDest)};

        xAnimation[(percentageLength * (i + 1)).toString() + "%"] = 
                        {cx: grid[waypoint.x][waypoint.y].pos.x, easing: "linear"};

    }

    // Perform animation
    car.stop().animate(yAnimation, totalTime).animate(xAnimation, totalTime);
}

$(document).ready(function(){
    
    // Initially hide run button
    $("#run").hide();
    
    // Set click handler
    $("#run").click(function(){
        $("#run").hide();
        if(!tripSet)
            setTrip(tripArray[0], doTrip);
        else
            doTrip();
    });

    Raphael(function () {
        // Create paper for canvas        
        paper = Raphael("holder");

        // Setup waypoint grid
        grid = new Array(10);
        for(var i=0; i < 10; i++){
            grid[i] = new Array(10);
            for(var j=0; j < 10; j++){
                pos = {x : 50 + i*50, y : 50 + j*50};
                grid[i][j] = {circle : paper.circle(pos.x, pos.y, 8), pos : pos};
                grid[i][j].circle.attr({stroke: "none", fill: "#666"});
            }
        }
        
        // Load the trips
        loadTrips();
    });
});

        