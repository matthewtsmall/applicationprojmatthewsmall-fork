﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using System.IO;

namespace TransportProj
{

    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;
            
            Console.WriteLine("Would you like a (S)edan or (R)aceCar?");
            ConsoleKeyInfo keyInfo = Console.ReadKey();

            CarType carType;
            if (keyInfo.KeyChar.ToString().ToLower() == "r")
                carType = CarType.RaceCar;
            else
                carType = CarType.Sedan;
      
            City MyCity = new City(CityLength, CityWidth);
            Car car = MyCity.AddCarToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), carType);
            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            Point carStart = new Point(car.XPos, car.YPos);
            Console.WriteLine(String.Format(""));
            Console.WriteLine(String.Format("Passenger at x - {0} y - {1}", passenger.GetCurrentXPos(), passenger.GetCurrentYPos()));
            Console.WriteLine(String.Format("Destination at x - {0} y - {1}", passenger.DestinationXPos, passenger.DestinationYPos));
            Console.WriteLine(String.Format("Car at x - {0} y - {1}", car.XPos, car.YPos));
            Console.WriteLine(String.Format("Speed {0}", car.Speed));

            // Create an http client
            //HttpClient client = new HttpClient();

            while (!passenger.IsAtDestination())
            {
                // Start the task
                //Task<string> getStringTask = client.GetStringAsync("http://2pointB.com");

                // Perform car movement
                Tick(car, passenger);

                // Block on website download
                //string result  = getStringTask.Result;
            }

            // Create BSON array from waypoints
            BsonArray bWaypoints = new BsonArray();
            foreach (var waypoint in car.GetWaypoints())
                bWaypoints.Add(waypoint.ToBsonDocument());
           

            // Generate new document
            var document = new BsonDocument
            {
                { "carType" , carType.ToString()},
                { "carStart", carStart.ToBsonDocument()},
                { "passengerStart", new Point(passenger.StartingXPos, passenger.StartingYPos).ToBsonDocument()},
                { "passengerDestination", new Point(passenger.DestinationXPos, passenger.DestinationYPos).ToBsonDocument()},
                { "waypoints", bWaypoints }
            };

            // Insert document into mongolab DB
            var client = new MongoClient("mongodb://backend:47LVRvhr3UCh5fsE@ds041623.mongolab.com:41623/2pointb-gauntlet");
            var database = client.GetDatabase("2pointb-gauntlet");
            var insertTask = database.GetCollection<BsonDocument>("trips").InsertOneAsync(document);
            insertTask.Wait();

            Console.WriteLine("Finished with success! Press a key to see a simulation.");
            Console.ReadKey();
            
            string absolute = Path.GetFullPath("../../front-end/trip.html");
            System.Diagnostics.Process.Start("file:///" + absolute);

            return;
        }


        
        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            int targetX;
            int targetY;

            // Determine required movement vector
            if (car.Passenger == null) {
                targetX = passenger.GetCurrentXPos();
                targetY = passenger.GetCurrentYPos();
            }
            else {
                targetX = passenger.DestinationXPos;
                targetY = passenger.DestinationYPos;
            }

            int deltaX = targetX - car.XPos;
            int deltaY = targetY - car.YPos;
            

            // We have arrived at passenger or destination
            if((deltaX == 0) && (deltaY == 0)){
                if (car.Passenger == null)
                    passenger.GetInCar(car);
                else
                    passenger.GetOutOfCar();
                return;
            }

            // Move in axis with largest delta
            if (Math.Abs(deltaX) > Math.Abs(deltaY)) {

                int targetDirection = deltaX > 0 ? 1 : -1;

                // We started in an over-shoot position
                if (car is RaceCar && Math.Abs(deltaX) == 1) {

                    // We can move past the target an odd number of spaces
                    if (City.InBoundsX(car.XPos + (3 * targetDirection))){
                        car.SetSpeed(3);
                        if (targetDirection < 0)
                            car.MoveLeft();
                        else
                            car.MoveRight();
                        car.SetSpeed(2);
                    }
                    // We can move past the target 
                    else if (City.InBoundsX(car.XPos + (2 * targetDirection))) {
                        if (targetDirection < 0)
                            car.MoveLeft();
                        else
                            car.MoveRight();
                    }
                    // We can move away from target an odd number of spaces
                    else if (City.InBoundsX(car.XPos + (3 * targetDirection * -1)))
                    {
                        car.SetSpeed(3);
                        if (targetDirection < 0)
                            car.MoveRight();
                        else
                            car.MoveLeft();
                        car.SetSpeed(2);
                    }
                    // We can move away from the target
                    else if (City.InBoundsX(car.XPos + (2 * targetDirection * -1))) {
                        if (targetDirection < 0)
                            car.MoveLeft();
                        else
                            car.MoveRight();
                    }
                    // Bad city map
                    else
                    {
                        Console.WriteLine("City map not solveable.");
                        return;
                    }
                }
                // Avoid over-shoot position
                else if (car is RaceCar && Math.Abs(deltaX) == 3)
                {
                    car.SetSpeed(3);
                    if (targetDirection < 0)
                        car.MoveLeft();
                    else
                        car.MoveRight();
                    car.SetSpeed(2);
                }
                // Normal move right
                else if (deltaX > 0)
                    car.MoveRight();
                // Normal move left
                else
                    car.MoveLeft();
            }
            else
            {

                int targetDirection = deltaY > 0 ? 1 : -1;

                // We started in an over-shoot position
                if (car is RaceCar && Math.Abs(deltaY) == 1)
                {

                    // We can move past the target an odd number of spaces
                    if (City.InBoundsY(car.YPos + (3 * targetDirection))){
                        car.SetSpeed(3);
                        if (targetDirection < 0)
                            car.MoveDown();
                        else
                            car.MoveUp();
                        car.SetSpeed(2);
                    }
                    // We can move past the target 
                    else if (City.InBoundsY(car.YPos + (2 * targetDirection)))
                    {
                        if (targetDirection < 0)
                            car.MoveDown();
                        else
                            car.MoveUp();
                    }
                    // We can move away from target an odd number of spaces
                    else if (City.InBoundsY(car.YPos + (3 * targetDirection * -1)))
                    {
                        car.SetSpeed(3);
                        if (targetDirection < 0)
                            car.MoveUp();
                        else
                            car.MoveDown();
                        car.SetSpeed(2);
                    }
                    // We can move away from the target
                    else if (City.InBoundsY(car.YPos + (2 * targetDirection * -1)))
                    {
                        if (targetDirection < 0)
                            car.MoveDown();
                        else
                            car.MoveUp();
                    }
                    // Bad city map
                    else
                    {
                        Console.WriteLine("City map not solveable.");
                        return;
                    }
                }
                // Avoid over-shoot position
                else if (car is RaceCar && Math.Abs(deltaY) == 3)
                {
                    car.SetSpeed(3);
                    if (targetDirection < 0)
                        car.MoveDown();
                    else
                        car.MoveUp();
                    car.SetSpeed(2);
                }
                // Normal move right
                else if (deltaY > 0)
                    car.MoveUp();
                // Normal move left
                else
                    car.MoveDown();
            }
        }

    }
}
