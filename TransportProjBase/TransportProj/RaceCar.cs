﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class RaceCar : Car
    {
        public RaceCar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
            SetSpeed(2);
        }

        public override void SetSpeed(int speed)
        {
            Speed = Math.Max(2, speed);
        }

        public override void MoveUp()
        {
            if (City.InBoundsY(YPos + Speed))
            {
                YPos += Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveDown()
        {
            if (City.InBoundsY(YPos - Speed))
            {
                YPos -= Speed;
                WritePositionToConsole();
            }
        }

        public override void MoveRight()
        {
            if(City.InBoundsX(XPos + Speed))
            {
                XPos += Speed;
                WritePositionToConsole();
            }   
        }

        public override void MoveLeft()
        {
            if (City.InBoundsX(XPos - Speed))
            {
                XPos -= Speed;
                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            LogPosition();
            Console.WriteLine(String.Format("RaceCar moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}
