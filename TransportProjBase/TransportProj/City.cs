﻿
namespace TransportProj
{

    public class City
    {
        public static int YMax { get; private set; }
        public static int XMax { get; private set; }

        /// <summary>
        /// Check if x position is within city bounds
        /// </summary>
        /// <param name="xPos">The x position to be checked.</param>
        /// <returns></returns>
        public static bool InBoundsX(int xPos)
        {
            return xPos >= 0 && xPos < City.XMax;
        }

        /// <summary>
        /// Check if y position is within city bounds
        /// </summary>
        /// <param name="yPos">The y position to be checked.</param>
        /// <returns></returns>
        public static bool InBoundsY(int yPos)
        {
            return yPos >= 0 && yPos < City.YMax;
        }

        public City(int xMax, int yMax)
        {
            XMax = xMax;
            YMax = yMax;
        }

        public Car AddCarToCity(int xPos, int yPos, CarType type = CarType.Sedan)
        {
            return Car.CarFactory(xPos, yPos, this, null, type);
        }

        public Passenger AddPassengerToCity(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

            return passenger;
        }

    }
}
